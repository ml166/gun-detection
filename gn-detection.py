import cv2
import imutils
import datetime
import os
import requests


path = os.path.dirname(os.path.realpath(__file__))
gun_cascade = cv2.CascadeClassifier(path + '/cascade.xml')
camera = cv2.VideoCapture(0)

firstFrame = None
gun_exist = False

while True:
	
	ret, frame = camera.read()

	frame = imutils.resize(frame, width = 500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	
	gun = gun_cascade.detectMultiScale(gray, 1.3, 5, minSize = (200, 200))
	if len(gun) > 0:
		gun_exist = True

		#Send trigger to backend service
		api_url = 'https://ec2-15-236-212-100.eu-west-3.compute.amazonaws.com:8001'
		notify = requests.get(api_url)
		print(str(len(gun)) + " Gun(s) Detected")
		
	for (x, y, w, h) in gun:
		frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
		roi_gray = gray[y:y + h, x:x + w]
		roi_color = frame[y:y + h, x:x + w]

	if firstFrame is None:
		firstFrame = gray
		continue

	cv2.putText(frame, datetime.datetime.now().strftime("% A % d % B % Y % I:% M:% S % p"), (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
	cv2.imshow("Security Feed", frame)
	key = cv2.waitKey(1) & 0xFF
 	
	if key == ord('q'):
		break

else:
	print("guns NOT detected")

camera.release()
cv2.destroyAllWindows()
