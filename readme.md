# Simple Gun Detection Using Open CV
## Setting Up
1. Create a virtual Enviroment, use the resource here to setup one.
https://docs.python.org/3/library/venv.html

2. Activate your virtual enviroment in the terminal.


3. Install the require dependencies under requirements.txt.
Run `pip install -r requirements.txt` or `pip3 install -r requirements.txt` depending on your python version.

4. Run the gun detection script, `python gn-detection.py`, move any image with a gun before your camera, a log of gun detected should appear in your terminal.